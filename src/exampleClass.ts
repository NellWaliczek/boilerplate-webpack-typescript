export default class ExampleClass {
  private text: string;

  constructor(text: string) {
    this.text = text;
  }

  get pageName(): string {
    return this.text;
  }
}
