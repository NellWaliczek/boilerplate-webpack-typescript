import ExampleClass from './exampleClass';

const exampleInstance: ExampleClass = new ExampleClass(
  'Boilerplate for typescript package built via webpack using eslint, prettier, and jest'
);

const element = document.createElement('div');
element.innerHTML = exampleInstance.pageName;
document.body.appendChild(element);
