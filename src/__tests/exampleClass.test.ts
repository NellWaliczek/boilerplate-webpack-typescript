import ExampleClass from '../exampleClass';

test('Example test', () => {
  const exampleInstance = new ExampleClass('test name');
  expect(exampleInstance.pageName).toBe('test name');
});
